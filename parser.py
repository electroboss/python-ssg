import json, yaml
import logging,sys

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

if __name__ == "__main__":
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

def jsonparse(file):
    if file == "": return ""
    try:
        filejson = json.loads(file)
    except:
        filejson = yaml.load(file,Loader=Loader)

    outfile = {
        "template": None,
        "contents": "",
    }

    if filejson["template"] != None:
        outfile = jsonparse(open("templates/"+filejson["template"],"r").read())
        outfile["contents"] = parsecontents(outfile["contents"],filejson)

    else:
        outfile = filejson
        outfile["contents"] = parsecontents(outfile["contents"],None)

    return outfile

def parse(file):
    outfilejson = jsonparse(file)

    outfile = ""

    for element in outfilejson["contents"]:
        logging.debug(element)
        outfile += element["contents"]
    
    return outfile

def parseelement(element,filejson):
    if type(element) == str:
        return {"type":"html","contents":element}
    if element["type"] == "html" or element["type"] == "raw":
        return element
    elif element["type"] == "part":
        if filejson == None:
            return element
        if not element["name"] in filejson["parts"].keys():
            return element
        return filejson["parts"][element["name"]]
    elif element["type"] == "ref":
        return {"type": "html", "contents": parse(open(element["ref"],"r").read())}

def parsecontents(contents, filejson):
        i = -1
        new_contents = list()
        for element in contents:
            i += 1
            new_element = parseelement(element, filejson)
            if type(new_element) == list:
                for element in parsecontents(new_element,None):
                    new_contents.append(element)
            else:
                new_contents.append(parseelement(new_element,None))
        return new_contents
