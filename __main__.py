import os
import parser
import logging
import sys
import argparse

argparser = argparse.ArgumentParser(description="YAML and JSON-based Static Site Generator (SSG) written in python 3.")
argparser.add_argument("-v", "--verbose", help="verbose mode, sets logging level to DEBUG", action="store_true")
argparser.add_argument("-q", "--quiet", help="quiet mode, sets logging level to ERROR", action="store_true")
argparser.add_argument("-o", "--output", type=str, default="out/", help="output folder")

args = argparser.parse_args()
logging_level = logging.ERROR if args.quiet else logging.DEBUG if args.verbose else logging.INFO

logging.basicConfig(stream=sys.stderr, level=logging_level)

logging.debug(args)

def changeextension(filename, extension):
    filename = filename.split(".")
    filename[-1] = extension
    filenames = str()
    for part in filename:
        filenames += part+"."
    return filenames[:-1]

if __name__ == "__main__":
    logging.info("Checking if output folder contents exists")
    if len(os.listdir(args.output)) != 0:
        logging.critical(args.output+" not empty!")
        raise FileExistsError(args.output+" not empty!")

    for (dirpath, dirnames, filenames) in os.walk("pages"):
        logging.info("%s, %s, %s" % (dirpath, dirnames, filenames))
        if not os.path.isdir(args.output+dirpath[5:]): os.mkdir(args.output+dirpath[5:])
        for filename in filenames:
            outfile = open(args.output+dirpath[5:]+"/"+filename[:-5],"w+")
            outfile.write(parser.parse(open(dirpath+"/"+filename,"r").read()))
            outfile.close()

    os.mkdir(args.output+"/static")

    for (dirpath, dirnames, filenames) in os.walk("static"):
        if not os.path.isdir(dirpath.replace("static",args.output+"/static")): os.mkdir(dirpath.replace("static", args.output+"/static"))
        for filename in filenames:
            open(dirpath.replace("static",args.output+"/static")+"/"+filename,"wb+").write(open(dirpath+"/"+filename,"rb").read())
